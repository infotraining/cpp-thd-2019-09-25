#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <vector>
#include "../utils.h"
#include <celero/Celero.h>

using namespace std;

CELERO_MAIN

class logger
{
    ofstream fout_;

public:
    logger(const std::string& filename)
    {
        fout_.open(filename);
    }

    void log(const string& message)
    {
        fout_ << message << endl;
    }
};

class logger_ao
{
    ofstream fout_;
    active_object ao_;

public:
    logger_ao(const std::string& filename)
    {
        fout_.open(filename);
    }

    ~logger_ao()
    {
//        ao_.dispatch([this]() {
//            fout_.close() ;}
//        );
    }

    void log(const string& message)
    {
        ao_.dispatch([message, this]() {
            //this_thread::sleep_for(1ms);
            fout_ << message << endl;
        });
    }
};

template <typename L>
void logging(L& log, int id)
{
    for (int i = 0 ; i < 10000 ; ++i)
    {
        //log.log("Log# " + to_string(id) + " event " + to_string(i));
        log.log("Log# ");
    }
}

logger lg1("one.txt");
logger_ao lg2("two.txt");

BASELINE(log, classic, 0, 0)
{

    celero::DoNotOptimizeAway( []() { logging(lg1, 1); } );
}


BENCHMARK(log, ao, 0, 0 )
{
    celero::DoNotOptimizeAway( []() { logging(lg2, 1); } );
}

//int main()
//{
//    logger log("messages.txt");
//    vector<thread> thds;

//    for (int i = 0 ; i < 8 ; ++i)
//        thds.emplace_back(logging_for_fun, ref(log), i);

//    for (auto& th : thds) th.join();
//    return 0;
//}
