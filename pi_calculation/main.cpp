#include <iostream>
#include <random>
#include <thread>
#include <mutex>
#include <atomic>
#include <celero/Celero.h>

CELERO_MAIN

using namespace std;

mutex mtx;

void check_hits_with_mutex_1(unsigned long N, unsigned long& hits)
{
    random_device rd;
    mt19937_64 gen(1);
    uniform_real_distribution<> dis(-1,1);
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        lock_guard lg{mtx};
        if (x * x + y * y < 1) ++hits;
    }
}

void check_hits_with_mutex_2(unsigned long N, unsigned long& hits)
{
    random_device rd;
    mt19937_64 gen(1);
    uniform_real_distribution<> dis(-1,1);
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x * x + y * y < 1)
        {
            lock_guard lg{mtx};
            ++hits;
        }
    }
}

void check_hits_with_atomic(unsigned long N, atomic<unsigned long>& hits)
{
    random_device rd;
    mt19937_64 gen(1);
    uniform_real_distribution<> dis(-1,1);
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x * x + y * y < 1)
        {
            ++hits;
        }
    }
}

void check_hits(unsigned long N, unsigned long& hits)
{
    random_device rd;
    mt19937_64 gen(1);
    uniform_real_distribution<> dis(-1,1);
    unsigned long counter{0};
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x * x + y * y < 1) ++counter;
    }
    hits = counter;
}

double single_thread_pi(unsigned long N)
{
    unsigned long hits = 0;
    check_hits(N, hits);
    return (static_cast<double>(hits)/N)*4;
}

double multithread_pi(unsigned long N, int threads,
                      std::function<void(unsigned long, unsigned long&)> fn)
{
    vector<unsigned long> hits(threads);
    //unsigned long hits{0};
    vector<thread> thds;
    for (int i = 0 ; i < threads ; ++i)
        thds.emplace_back(fn, N/threads, ref(hits[i]));

    for (auto& th : thds) th.join();
    return (accumulate(hits.begin(), hits.end(), 0.0)/N)*4;
    //return (static_cast<double>(hits)/N)*4;
}

double multithread_pi_atomic(unsigned long N, int threads)
{
    vector<unsigned long> hits(threads);
    atomic<unsigned long> counter;
    vector<thread> thds;
    for (int i = 0 ; i < threads ; ++i)
        thds.emplace_back(check_hits_with_atomic, N/threads, ref(counter));

    for (auto& th : thds) th.join();
    return (accumulate(hits.begin(), hits.end(), 0.0)/N)*4;
    //return (static_cast<double>(hits)/N)*4;
}

//int main()
//{
//    cout << "Hello World!" << endl;
//    cout << std::thread::hardware_concurrency() << endl;
//    long N = 10'000'000;
//    cout << "Pi = " << single_thread_pi(N) << endl;
//    cout << "Pi = " << multithread_pi(N, 8) << endl;
//    return 0;
//}

constexpr int n_of_samples = 2;
constexpr int n_of_iterations = 5;
constexpr unsigned long N = 1'000'000;

BASELINE(Pi, SingleThread, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(single_thread_pi(N));
}

BENCHMARK(Pi, Multithread2, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(multithread_pi(N, 2, check_hits));
}

BENCHMARK(Pi, Multithread4, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(multithread_pi(N, 4, check_hits));
}

BENCHMARK(Pi, Multithread8, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(multithread_pi(N, 8, check_hits));
}

BENCHMARK(Pi, Multithread16, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(multithread_pi(N, 16, check_hits));
}

BENCHMARK(Pi, Multithread8mutex1, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(multithread_pi(N, 8, check_hits_with_mutex_1));
}

BENCHMARK(Pi, Multithread8mutex2, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(multithread_pi(N, 8, check_hits_with_mutex_2));
}

BENCHMARK(Pi, Multithread8atomic, n_of_samples, n_of_iterations)
{
    celero::DoNotOptimizeAway(multithread_pi_atomic(N, 8));
}

