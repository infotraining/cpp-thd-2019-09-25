#ifndef UTILS_H
#define UTILS_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <functional>

template <typename T>
class thread_safe_queue
{
    std::queue<T> q_;
    std::mutex mtx_;
    std::condition_variable cond_;
public:
    thread_safe_queue() {}
    void push(T&& item)
    {
        std::lock_guard lg(mtx_);
        q_.push(std::move(item));
        cond_.notify_one();
    }

    T pop()
    {
        std::unique_lock ul(mtx_);
        while (q_.empty())
            cond_.wait(ul);
        T item =  q_.front();
        q_.pop();
        return item;
    }

    void pop(T& item)
    {
        std::unique_lock ul(mtx_);
        while (q_.empty())
            cond_.wait(ul);
        item =  q_.front();
        q_.pop();
    }

    std::pair<bool, T> pop_nowait()
    {
        std::unique_lock ul(mtx_);
        if (!q_.empty())
        {
            T item =  q_.front();
            q_.pop();
            return std::make_pair<bool, T>(true, item);
        }
        else {
            return std::make_pair<bool, T>(false, T());
        }
    }
};

using task_t = std::function<void()>;

class active_object
{
    std::thread dispatcher_;
    thread_safe_queue<task_t> list_;

    void run()
    {
        while(true)
        {
            task_t task = list_.pop();
            if (task) task();
            else return;
        }
    }
public:
    active_object() : dispatcher_(&active_object::run, this)
    {
    }
    ~active_object()
    {
        list_.push(nullptr);
        dispatcher_.join();
    }
    void dispatch(task_t task)
    {
        list_.push(move(task));
    }
};

#endif // UTILS_H
