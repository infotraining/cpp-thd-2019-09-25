#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>

using namespace std;

unsigned long counter{0};
std::mutex mtx;
std::atomic<unsigned long> atomic_counter{0};

void inc_counter(unsigned long N)
{
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        lock_guard<mutex> lg{mtx};
        ++counter;
    }
}

void inc_atomic_counter(unsigned long N)
{
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        ++atomic_counter;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> thds;
    for (int i = 0 ; i < 10 ; ++i)
    {
        thds.emplace_back(inc_atomic_counter, 1'000'000);
    }

    for (auto& th : thds)
        th.join();

    cout << atomic_counter.is_lock_free() << " is lock free?" << endl;
    cout << atomic_counter.is_always_lock_free << " is always lock free?" << endl;
    cout << atomic_counter << endl;
    return 0;
}
