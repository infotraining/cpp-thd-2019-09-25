#include <iostream>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <queue>

using namespace std;

int value{-1};
atomic<bool> is_ready{false};

condition_variable cond;
mutex mtx;
queue<int> q;

void producer()
{
    this_thread::sleep_for(30s);
    value = 42;
    is_ready = true;
}

void consumer(int id)
{
    while (!is_ready)
        continue;

    cout << "got value: " << value << endl;
}

void producer_cond()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(1s);
        lock_guard lg(mtx);
        q.push(i);
        cout <<"produced " << i << endl;
        cond.notify_one();
    }
}

void consumer_cond(int id)
{
    while(true)
    {
        unique_lock ul(mtx);
        while (q.empty())
            cond.wait(ul);
        cout << id << " got value: " << q.front() << endl;
        q.pop();
    }
}



int main()
{
    cout << "Hello World!" << endl;
    thread prod_thd{producer_cond};
    this_thread::sleep_for(2s);
    thread cons_thd1{consumer_cond, 1};
    thread cons_thd2{consumer_cond, 2};
    thread cons_thd3{consumer_cond, 3};
    thread cons_thd4{consumer_cond, 4};
    prod_thd.join();
    cons_thd1.join();
    cons_thd2.join();
    cons_thd3.join();
    cons_thd4.join();
    return 0;
}
