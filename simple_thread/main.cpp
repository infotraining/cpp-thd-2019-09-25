#include <iostream>
#include <thread>

using namespace std;

void hello_fun(void)
{
    cout << "Hello from thread" << endl;
    this_thread::sleep_for(5s);
}

class Background_worker
{
    int id_;
public:

    void operator()(int id, const std::string& text, chrono::milliseconds delay)
    {
        for (auto ch : text)
        {
            cout << "id: " << id  << ": " << ch << endl;
            this_thread::sleep_for(delay);
        }
    }
};

int main()
{
    cout << "Hello World!" << endl;
    std::thread thd{hello_fun};
    cout << "after thread launch" << endl;

    Background_worker bw{};
    std::thread thd2{bw, 1, "Ala ma kota", 100ms};

    std::thread thd3{[]() {cout << "from lambda " << endl;}};

    std::thread thd4 = std::move(thd);

    if (thd.joinable())
        thd.join();
    thd2.join();
    thd3.join();
    thd4.join();

    cout << "after join" << endl;
    return 0;
}
