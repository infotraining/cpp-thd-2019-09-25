#include <iostream>
#include <thread>
#include "thread_guard.hpp"

using namespace std;

void worker(int id)
{
    cout << "Worker " << id << endl;
    this_thread::sleep_for(1s);
}

std::thread create_worker(int id)
{
    thread thd(worker, id);
    return thd;
}

int main()
{
    cout << "Hello World!" << endl;
    std::thread thd = create_worker(1);

    std::thread thd2{worker, 2};

    //thd.join();

    thread_guard tg(std::move(thd));
    thread_guard tg2(std::thread(worker,3));
    thread_guard tg3(create_worker(4));
    thread_guard tg4(worker, 5);
    thd2.join();
    return 0;
}
