#ifndef THREAD_GUARD_HPP
#define THREAD_GUARD_HPP
#include <thread>

class thread_guard
{
    std::thread thd_;
public:
    template <typename Callable, typename... Args,
                  typename = ::std::enable_if_t<!::std::is_same_v<::std::decay_t<Callable>, thread_guard>>>
    explicit thread_guard(Callable&& cb, Args&&... args)
      : thd_(std::forward<Callable>(cb), std::forward<Args>(args)...)
    {

    }

    ~thread_guard()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

#endif // THREAD_GUARD_HPP
