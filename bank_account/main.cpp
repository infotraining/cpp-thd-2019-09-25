#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

class bank_account
{
    const int id_;
    double balance_;
    mutex mtx_;

public:
    bank_account(int id, double balance) : id_(id), balance_(balance)
    {}

    void print() const
    {
        cout << "id: " << id_ << " balance: " << balance_ << endl;
    }

    void deposit(double amount)
    {
        lock_guard lg{mtx_};
        balance_ += amount;
    }

    void transfer(bank_account& to, double amount)
    {
        unique_lock ul1(mtx_, std::defer_lock);     // C++11
        unique_lock ul2(to.mtx_, std::defer_lock);
        std::lock(ul1, ul2);
        //std::scoped_lock sl(mtx_, to.mtx_); /// C++17 only
        balance_ -= amount;
        to.balance_ += amount;
    }
};

void make_transfers(bank_account& acc1, bank_account& acc2, int n_of_op, double amount)
{
    for (int i = 0 ; i < n_of_op ; ++i )
        acc1.transfer(acc2, amount);
}

void make_deposit(bank_account& acc, int n_of_op, double amount)
{
    for (int i = 0 ; i < n_of_op ; ++i )
        acc.deposit(amount);
}

int main()
{
    cout << "Hello Bank World!" << endl;
    bank_account ba1(1, 1000);
    bank_account ba2(2, 5000);
    thread th3(make_transfers, ref(ba1), ref(ba2), 1000, 1000);
    thread th4(make_transfers, ref(ba2), ref(ba1), 1000, 1000);
    thread th1(make_deposit, ref(ba1), 1000, 1000);
    thread th2(make_deposit, ref(ba1), 1000, -1000);
    th1.join();
    th2.join();
    th3.join();
    th4.join();

    ba1.print();
    ba2.print();
    return 0;
}
