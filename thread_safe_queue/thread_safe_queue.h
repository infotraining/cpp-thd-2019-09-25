#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>

template <typename T>
class thread_safe_queue
{
    std::queue<T> q_;
    std::mutex mtx_;
    std::condition_variable cond_;
public:
    thread_safe_queue() {}
    void push(T&& item)
    {
        std::lock_guard lg(mtx_);
        q_.push(std::move(item));
        cond_.notify_one();
    }

    T pop()
    {
        std::unique_lock ul(mtx_);
        while (q_.empty())
            cond_.wait(ul);
        T item =  q_.front();
        q_.pop();
        return item;
    }

    void pop(T& item)
    {
        std::unique_lock ul(mtx_);
        while (q_.empty())
            cond_.wait(ul);
        item =  q_.front();
        q_.pop();
    }

    std::pair<bool, T> pop_nowait()
    {
        std::unique_lock ul(mtx_);
        if (!q_.empty())
        {
            T item =  q_.front();
            q_.pop();
            return std::make_pair<bool, T>(true, item);
        }
        else {
            return std::make_pair<bool, T>(false, T());
        }
    }
};

#endif // THREAD_SAFE_QUEUE_H
