#include <iostream>
#include "thread_safe_queue.h"
#include <thread>
#include <vector>

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for (int i = 0  ; i < 100 ; ++i)
    {
        q.push(std::move(i));
        this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    while (true) {
        int i = q.pop();
        // q.pop_nowait();
        cout << id << " consumer got " << i << endl;
    }

}

int main()
{
    vector<thread> thds;
    thds.emplace_back(producer);
    for (int i = 0 ; i < 4 ; ++i)
        thds.emplace_back(consumer, i);
    for (auto& th : thds) th.join();
    return 0;
}
