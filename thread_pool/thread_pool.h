#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include "../utils.h"
#include <functional>
#include <thread>
#include <vector>
#include <future>

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> tasks_;
    std::vector<std::thread> pool_;

    void worker()
    {
        while(true)
        {
            auto task = tasks_.pop();
            if (task)
                task();
            else {
                return;
            }
        }
    }
public:
    thread_pool(size_t n_of_threads)
    {
        // create pool of threads
        for (size_t i = 0 ; i < n_of_threads ; ++i)
        {
            pool_.emplace_back(&thread_pool::worker, this);
        }
    }
    auto add(task_t task) -> void
    {
        // add task to queue
        tasks_.push(move(task));
    }

    template<typename Task>
    auto add(Task&& task)// -> std::future<decltype (task())>
    {
        using RetType = decltype (task());
        auto pt = std::make_shared<std::packaged_task<RetType()>>(std::forward<Task>(task));
        auto result = pt->get_future();
        tasks_.push([pt]() {(*pt)();});
        return result;
    }

    ~thread_pool()
    {
        // join pool of threads
        for (auto& th : pool_) tasks_.push(nullptr);
        for (auto& th : pool_) th.join();
    }
};

#endif // THREAD_POOL_H
