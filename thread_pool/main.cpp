#include <iostream>
#include "thread_pool.h"

using namespace std;


void add(int a, int b)
{
    this_thread::sleep_for(100ms);
    cout << a + b << endl;
}

void fun()
{
    cout << "fun output" << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    thread_pool tp{std::thread::hardware_concurrency()};
    thread_safe_queue<int> output;
    tp.add( []() -> void {cout << "Hello from tp" << endl;});
    tp.add(fun);
    //for( int i = 0 ; i < 100 ; ++i)
    //    tp.add([=]() {this_thread::sleep_for(100ms); cout << i << endl;});
    int a = 3;
    int b = 4;
    tp.add( [=]() -> void {add(a, b);});
    tp.add([&output]() {output.push(123);});
    cout << "adding finished" << endl;

    auto result = tp.add([](){return 42;});

    cout << "Result from tp-future" << endl;
    cout << result.get() << endl;

    thread watcher([&output]() { while(true){
            cout << output.pop() << endl;
        }});

    watcher.join();
    return 0;
}
