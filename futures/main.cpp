#include <iostream>
#include <thread>
#include <future>

using namespace std;

int calculate_something(int a)
{
    this_thread::sleep_for(1s);
    return a*2;
}

int may_throw()
{
    throw  std::runtime_error("error");
    return 13;
}

int main()
{
    int result;
    result = calculate_something(10);
    cout << result << endl;

    std::future<int> fut = async(launch::async, calculate_something, 2);
    std::future<int> fut_with_exception = async(launch::async, may_throw);
    cout << "waiting for result" << endl;
    //fut.wait();
    cout << "result is ready"  << endl;
    //cout << fut.get() << endl;
    try {
        fut_with_exception.get();
    } catch (std::runtime_error& err) {
        cout << "Got exception: " << err.what() << endl;
    }

    std::packaged_task<int(int)> pt(calculate_something);
    std::future<int> fut_pt = pt.get_future();
    std::thread thd(move(pt), 4);
    cout << "future from package task " << fut_pt.get() << endl;
    thd.join();

    return 0;
}
