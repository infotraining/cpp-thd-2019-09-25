#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void can_throw(int id, exception_ptr& eptr)
{
    this_thread::sleep_for(1s);
    try {
            throw std::runtime_error("out of luck");
    } catch (...) {
        cout << "exception :(" << endl;
        eptr = current_exception();
    }

}

int main()
{
    exception_ptr eptr;
    cout << "Hello World!" << endl;
    thread thd(can_throw,1, ref(eptr));
    thd.join();
    try {
        if(eptr) rethrow_exception(eptr);

    } catch (std::runtime_error& err) {
        cout << "Exception!! " << err.what() << endl;
    }
    return 0;
}
