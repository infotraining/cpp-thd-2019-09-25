#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <execution>
#include <celero/Celero.h>

CELERO_MAIN

using namespace std;

std::string load_file(const std::filesystem::path& path)
{
    std::ifstream in(path);
    if (in)
    {
        std::string content;
        in.seekg(0, std::ios::end);
        content.resize(static_cast<size_t>(in.tellg()));
        in.seekg(0, std::ios::beg);
        in.read(content.data(), content.size());
        in.close();
        return content;
    }
    else {
        return std::string{};
    }

}

template  <typename EP>
unsigned long count_words(const std::string& text, EP policy)
{
    unsigned long wc = 0;
    if (text.size() == 0) return 0;

    auto is_word_beginning  = [](auto left, auto right)
    {
        return isspace(left) && !isspace(right);
    };
    wc += transform_reduce(policy,
                           text.begin(),
                           text.end()-1,
                           text.begin()+1,
                           std::size_t{0},
                           std::plus<>(),
                           is_word_beginning);

    return wc;
}

auto text = load_file("../count_words/pg7178.txt");

BASELINE(STL, count_seq, 10, 10)
{
    celero::DoNotOptimizeAway(count_words(text, std::execution::seq));
}

BENCHMARK(STL, par_unseq, 10, 10 )
{
    celero::DoNotOptimizeAway(count_words(text, std::execution::par_unseq));
}
