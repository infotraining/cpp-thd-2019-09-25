#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>

using namespace std;

unsigned long counter{0};

class spin_lock
{
    std::atomic_flag flag_;
public:
    spin_lock() : flag_{ATOMIC_FLAG_INIT}
    {

    }

    void lock()
    {
        while(flag_.test_and_set(std::memory_order_acquire))
            continue;
    }

    void unlock()
    {
        flag_.clear();
    }
};

spin_lock mtx;

void inc_counter(unsigned long N)
{
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        mtx.lock();
        ++counter;
        mtx.unlock();
    }
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> thds;
    for (int i = 0 ; i < 10 ; ++i)
    {
        thds.emplace_back(inc_counter, 1'000'000);
    }

    for (auto& th : thds)
        th.join();

    cout << counter << endl;
    return 0;
}
